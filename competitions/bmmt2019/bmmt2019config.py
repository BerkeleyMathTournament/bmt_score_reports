from base_controller import BaseController
from models.contest import Contest
from statistics import StatisticEnum
import os
import argparse

class BmMT2019Controller(BaseController):
    def initialize(self):
        """
        Sets all relevant data for BmMT 2019
        :return:
        """
        path = os.path.join(os.path.dirname(__file__), 'csvs')
        self.team_csv = os.path.join(path, "team.csv")

        self.individual_contests = [Contest(20, 20, 'Geometry', 8, self.individual_csv)]
        self.team_contests = [Contest(20, 20, 'Guts', 30, os.path.join(path, "team.csv")),
                              Contest(0, 150, 'Power', 20, os.path.join(path, "puzzle.csv")),
                              ]

        self.statistics = [stat.value for stat in StatisticEnum]
        self.output_dir = os.path.join(os.path.dirname(path), 'output_dir')
        self.tournament_name = "BmMT 2019 Iran"
        self.introductory_text = "Thank you for coming to BmMT 2019 Iran. We had over 200 students participate this year. This is " \
                                 "our largest tournament yet in Iran! This would not have been possible without the help of our sponsors, our volunteers" \
                                 "parents, and coaches! Thank you all for a successful tournament!"


    def parse_team_info(self, team_info):
        return {'id': self.get_team_id(team_info['ID']), 'name': team_info['Team Name']}

    def parse_individual_info(self, info):
        return {'id': info['ID'], 'name': info['StudentName']}

    def parse_individual_contest_info(self, info, contest):
        return {'id': info['ID'],
                'scan_url': info['ViewImageURL'],
                'scores': [info[f"Problem{i}"] for i in range(1, contest.num_problems + 1)],
                'total_score': info['TotalScore']
                }

    def parse_team_contest_info(self, team_info, contest):
        if contest.name == 'Puzzle':
            return {
                    'id': self.get_team_id(team_info['\ufeffID']),
                    'scores': [team_info[f"Problem {i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore'],
                }
        else:
            return {
                    'id': team_info['ID'],
                    'scores': [team_info[f"Problem{i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore'],
                    'scan_url': team_info['ViewImageURL'],
            }

    @staticmethod
    def get_team_id(num_id):
        num_zeroes = 4 - len(str(num_id))
        return '0' * num_zeroes + str(num_id)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process controller arguments.')
    parser.add_argument('--should_generate_report', metavar="-s", type=bool, default=False,
                        help='Should we generate reports or just results')

    args = parser.parse_args()

    controller = BmMT2019Controller()
    controller.output_overall_csv()
    if args.should_generate_report:
        controller.generate_score_reports()
