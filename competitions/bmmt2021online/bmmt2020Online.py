from base_controller import BaseController
from models.contest import Contest
from models.award import LaTeXCutoffAward, LaTeXOverallPlacementAward, LaTeXPlacementAward
from statistics import StatisticEnum
import os
import argparse
from utils import ThreshHoldStrategy


class BMT2020OnlineController(BaseController):

    def initialize(self):
        """
        Sets all relevant data for BmMT 2019
        :return:
        """
        path = os.path.join(os.path.dirname(__file__), 'data')

        # creates paths to csvs for the overall and individual rosters
        self.team_csv = os.path.join(path, "team_roster.csv")
        self.individual_csv = os.path.join(path, "individual_roster.csv")

        # answers
        pacer_answers = [7200, 49, -9, 31, 41, 98, 12, 100, 17, 4, 2, 120, 15, 1, 7, 77, 30, 150, 5, 1]

        # Creates individual contests and team contests. Note weight is a per-individual score

        individual = Contest(20, 20, 'Individual Round', 100, os.path.join(path, "individual.csv"))

        self.individual_contests = [individual]

        # Team Contests

        pacer = Contest(20, 20000, 'Pacer Round', 10, os.path.join(path, 'pacer.csv'), answers=pacer_answers)
        team = Contest(20, 20, 'Team Round', 30, os.path.join(path, 'team.csv'))
        puzzle = Contest(20, 200, 'Puzzle Round', 20, os.path.join(path, 'puzzle.csv'))

        self.team_contests = [team, puzzle, pacer]

        # Awards

        variables = ['AwardName', 'ContestName', 'Name']
        award_path = os.path.relpath("../latex_docs/certificate/main.tex", path)

        for contest in self.individual_contests:
            self.awards.append(LaTeXCutoffAward("Distinguished Honorable Mention", contest, award_path, variables, 20,
                                                strategy=ThreshHoldStrategy.EXCLUDE))

            self.awards.append(LaTeXCutoffAward("Honorable Mention", contest, award_path, variables, 50,
                                                strategy=ThreshHoldStrategy.INCLUDE))

        for contest in self.team_contests:
            self.awards.append(LaTeXPlacementAward(contest.name, contest, award_path, variables, list(range(1, 11))))

        self.awards.append(LaTeXOverallPlacementAward("BmMT 2021", self, award_path, variables, list(range(1, 21))))
        # gets all statistics that you would like to be generated
        self.statistics = [stat.value for stat in StatisticEnum if stat not in [StatisticEnum.HONORABLE_MENTION_CUTOFF]]
        self.output_dir = os.path.join(os.path.dirname(path), 'output_dir')

        # Sets the tournament name and
        self.tournament_name = "BmMT 2021 Online"
        self.max_num_students = 5
        self.min_num_students = 3
        self.individual_weight = 40

        # Note (introductory text assumes a Dear Coaches, heading" and an Ending to the letter as well.
        self.introductory_text = "Thank you for coming to BmMT 2021 Online. This was the largest BmMT ever, and it was online! There were " \
                                 "over 900 participants from all over the United States and Canada! We are very grateful " \
                                 "for your patience in receiving scores. If you would like to give feedback, " \
                                 "please fill out a form here: https://forms.gle/B8cjgh8UMWc3BF9A7. " \
                                 "Please let us know if you have any questions!"

    def parse_team_info(self, team_info):
        return {'id': team_info['ID'], 'name': team_info["Name"], 'institution': team_info.get('Institution', 'N/A')}

    def parse_individual_info(self, info):
        student_id = str(int(info['StudentID'][:-1])) +  info['StudentID'][-1]
        return {'id': student_id, 'name': info['StudentName']}

    def parse_individual_contest_info(self, info, contest):
        return {'id': info['Student ID'],
                'scores': [info[f"Problem {i}"] for i in range(1, contest.num_problems + 1)],
                'total_score': info['TotalScore']
                }

    def parse_team_contest_info(self, team_info, contest):
        if contest.name == 'Team Round':
            return {'id': team_info['Team ID '],
                    'scores':  [team_info[f"Problem {i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore']}

        elif contest.name == 'Puzzle Round':
            return {
                    'id': team_info['Team ID '],
                    'total_score': team_info['TotalScore'],
            }
        else:
            return {
                    'id': team_info['Team ID '],
                    'answers': [team_info[f"Answer {i}"] for i in range(1, contest.num_problems + 1)],
                    'scores': [team_info[f"Problem {i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore'],
            }


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process controller arguments.')
    parser.add_argument('--should_generate_report', metavar="-s", type=bool, default=False, required=False,
                        help='Should we generate reports or just results')

    args = parser.parse_args()

    controller = BMT2020OnlineController()
    controller.output_overall_csv()
    controller.give_awards()
    controller.generate_score_reports()
    controller.combine_awards()


