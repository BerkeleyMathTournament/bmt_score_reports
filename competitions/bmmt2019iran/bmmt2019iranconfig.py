from base_controller import BaseController
from models.contest import Contest
from statistics import StatisticEnum
import os
import argparse


class BmMT2019IranController(BaseController):

    def initialize(self):
        """
        Sets all relevant data for BmMT 2019
        :return:
        """
        path = os.path.join(os.path.dirname(__file__), 'csvs')
        self.team_csv = os.path.join(path, "team.csv")
        self.individual_csv = os.path.join(path, "individual.csv")

        self.individual_contests = [Contest(20, 20, 'Individual', 8, self.individual_csv)]
        self.team_contests = [Contest(20, 20, 'Team', 30, os.path.join(path, "team.csv")),
                              Contest(15, 150, 'Puzzle', 20, os.path.join(path, "puzzle.csv")),
                              Contest(50, 50, 'Speed', 10, os.path.join(path, "speed.csv"))
                              ]

        self.statistics = [stat.value for stat in StatisticEnum]
        self.output_dir = os.path.join(os.path.dirname(path), 'output_dir')
        self.tournament_name = "BmMT 2019 Iran"
        self.introductory_text = "Thank you for coming to BmMT 2019 Iran. We had over 200 students participate this year. This is " \
                                 "our largest tournament yet in Iran! This would not have been possible without the help of our sponsors, our volunteers" \
                                 "parents, and coaches! Thank you all for a successful tournament!"

    def parse_team_info(self, team_info):
        return {'id': team_info['ID'], 'name': team_info['ID']}

    def parse_individual_info(self, info):
        return {'id': info['Tea ID'] + info['Name'][2], 'name': info['Full Name']}

    def parse_individual_contest_info(self, info, contest):
        return {'id': info['Tea ID'] + info['Name'][2],
                'scan_url': info['ViewImageURL'],
                'scores': [info[f"Problem{i}"] for i in range(1, contest.num_problems + 1)],
                'total_score': info['TotalScore']
                }

    def parse_team_contest_info(self, team_info, contest):
        if contest.name == 'Puzzle':
            return {'id': team_info['Name'], 'total_score': team_info['TotalScore']}
        else:
            return {
                    'id': team_info['Team ID'],
                    'scores': [team_info[f"Problem{i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore'],
                    'scan_url': team_info['ViewImageURL'],
            }


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process controller arguments.')
    parser.add_argument('--should_generate_report', metavar="-s", type=bool, default=False, required=False,
                        help='Should we generate reports or just results')

    args = parser.parse_args()

    controller = BmMT2019IranController()
    controller.output_overall_csv()
    if args.should_generate_report:
        controller.generate_score_reports()
