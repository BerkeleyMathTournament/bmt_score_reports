from base_controller import BaseController
from models.contest import Contest
from models.award import LaTeXCutoffAward, LaTeXOverallCutoffAward
from statistics import StatisticEnum
import os
import argparse
from utils import ThreshHoldStrategy


class BMT2020OnlineController(BaseController):

    def initialize(self):
        """
        Sets all relevant data for BmMT 2019
        :return:
        """
        path = os.path.join(os.path.dirname(__file__), 'csvs')

        # creates paths to csvs for the overall and individual rosters
        self.team_csv = os.path.join(path, "team.csv")
        self.individual_csv = os.path.join(path, "individual.csv")

        # answers

        indiv_answers = [216, 5, 4, 25, 100, 3, 3, 21600, 7, 5, 10, 50, 19, 18, 2023, 36, 173, 25, 156, 54, 2024, 82, 89, 2, 47]
        geo_answers = [100, 3, 5, 156, 3031, 252, 89, 629, 2029, 5362]
        alg_answers = [390, 202, 2023, 143, 47, 329, 77, 4, 113, 157]
        disc_answers = [96, 3, 50, 82, 2024, 2, 15, 27, 76, 54]
        calc_answers = [2019, 400, 5, 6060, 5, 1, 3, 11, 4040, 3]
        team_answers = [11, 16, 5, 103, 6, 155, 1297, 4, 5050, 294, 2331, 216, 1020605, 14, [1817, 2019], 21, 4, 3, 6, 8, 20, 69, 1926, 8078, 49, 183761, 277]
        # Creates individual contests and team contests. Note weight is a per-individual score

        individual = Contest(25, 25, 'Individual Round', 50, os.path.join(path, "general.csv"), answers=indiv_answers)
        geometry = Contest(10, 10, 'Geometry Round', 50, os.path.join(path, "geometry.csv"), answers=geo_answers)
        calculus = Contest(10, 10, 'Calculus Round', 50, os.path.join(path, "calculus.csv"), answers=calc_answers)
        algebra = Contest(10, 10, 'Algebra Round', 50, os.path.join(path, "algebra.csv"), answers=alg_answers)
        discrete = Contest(10, 10, 'Discrete Round', 50, os.path.join(path, "discrete.csv"), answers=disc_answers)
        self.individual_contests = [individual, geometry, calculus, algebra, discrete]

        # Team Contests
        team = Contest(27, 417, 'Team Round', 40, os.path.join(path, "guts.csv"), answers=team_answers)
        power = Contest(15, 150, 'Power Round', 0, os.path.join(path, "power.csv"))

        self.team_contests = [team, power]

        # Awards

        variables = ['AwardName', 'ContestName', 'Name']
        award_path = os.path.relpath("../latex_docs/certificate/main.tex", path)
        print(award_path)

        for contest in self.individual_contests:
            self.awards.append(LaTeXCutoffAward("Honorable Mention", contest, award_path, variables, 20,
                                                strategy=ThreshHoldStrategy.INCLUDE))

            self.awards.append(LaTeXCutoffAward("Top 50 Percent", contest, award_path, variables, 50,
                                                strategy=ThreshHoldStrategy.INCLUDE))

        for contest in self.team_contests:
            self.awards.append(LaTeXCutoffAward("Honorable Mention", contest, award_path, variables, 20,
                                                strategy=ThreshHoldStrategy.INCLUDE))

            self.awards.append(LaTeXCutoffAward("Top 50 Percent", contest, award_path, variables, 50,
                                                strategy=ThreshHoldStrategy.INCLUDE))

        self.awards.append(LaTeXOverallCutoffAward("Top 20 Percent", self,
                                                   award_path, variables, 20, strategy=ThreshHoldStrategy.INCLUDE))

        # gets all statistics that you would like to be generated
        self.statistics = [stat.value for stat in StatisticEnum]
        self.output_dir = os.path.join(os.path.dirname(path), 'output_dir')

        # Sets the tournament name and
        self.tournament_name = "BMT 2020 Online"
        self.max_num_students = 6
        self.min_num_students = 4
        self.individual_weight = 60

        # Note (introductory text assumes a Dear Coaches, heading" and an Ending to the letter as well.
        self.introductory_text = "Thank you for coming to BMT 2020 Online. This was perhaps are most successful BMT ever, and it was online! There were " \
                                 "over 1000 participants from all over the United States, Canada, and the World! We are very grateful for your patience in receiving scores" \
                                 "and are humbled by your feedback. Please let us know if you have any questions!"

        self.contest_weight_text = "Weights per point are calculated as the contest weight"\
                                   " divided by the average of either top 10\% of scores or the top 10 scores, depending "\
                                   "on which number (students in the top 10\% or 10) is bigger. "\
                                   "Additionally, note that the weight for the individual round is " \
                                   f"weighted by the total individual weight of {self.individual_weight}\%. "\
                                   f"Finally teams with 3 or more people had their indiviual scores averaged. "\
                                   f"If teams had less than 3 people, scores of 0 were added until a 3 person team was reached. "

    def parse_team_info(self, team_info):
        return {'id': team_info['\TeamID'], 'name': team_info['\TeamName'], 'institution': team_info['\InstitutionName']}

    def parse_individual_info(self, info):
        return {'id': info['Individual ID'], 'name': info['\StudentName']}

    def parse_individual_contest_info(self, info, contest):
        return {'id': info['IndivID'],
                'scores': [info[f"Correct {i}"] for i in range(1, contest.num_problems + 1)],
                'answers': [info[f"Question {i}"] for i in range(1, contest.num_problems + 1)],
                'total_score': info['TotalScore']
                }

    def parse_team_contest_info(self, team_info, contest):
        if contest.name == 'Power Round':
            scores = [sum([float(team_info[score]) for score in team_info.keys() if score[:len(str(i)) + 1] in [str(i) + ".", str(i) + ":"]])
                               for i in range(1, 16)]
            return {'id': team_info['Team ID'],
                    'scores': scores,
                    'total_score': team_info['TotalScore']}

        else:
            return {
                    'id': team_info['TeamID'],
                    'scores': [team_info[f"problem{i}"] for i in range(1, contest.num_problems + 1)],
                    'answers': [team_info[f"answerProblem{i}"] for i in range(1, contest.num_problems + 1)],
                    'total_score': team_info['TotalScore'],
            }


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process controller arguments.')
    parser.add_argument('--should_generate_report', metavar="-s", type=bool, default=False, required=False,
                        help='Should we generate reports or just results')

    args = parser.parse_args()

    controller = BMT2020OnlineController()
    controller.do()


