from models.team import Team
from models.student import Student
from models.score import ContestScore
from abc import ABC, abstractmethod
from collections import OrderedDict
import csv
from pathlib import Path
from pylatex import Document, Package, Command, NoEscape, Section, MiniPage, Tabular, LineBreak, NewPage, Figure
from pylatex.utils import bold
from pylatex.position import Center, VerticalSpace
import subprocess
import os
import numpy as np
from PyPDF2 import PdfFileMerger


class BaseController(ABC):
    """
    Controller class that generates the score reports given all of the information. This class should be subclassed for each
    tournament and its corresponding abstract methods filled in.
    """

    def __init__(self):
        self.teams = {}  # dictionary mapping from team ID to team
        self.students = {}  # dictionary mapping from student ID to student

        # ************************ To be set by initialize function *************************************
        self.team_csv = ""  # CSV for the overall results (i.e team name, team id)
        self.individual_csv = ""  # CSV for the individual roster
        self.team_contests = []  # List of Contest objects for each team contest
        self.individual_contests = []  # List of Contest objects for each individual contest
        self.statistics = []  # List of statistics to compute for each Contest
        self.output_dir = Path(__file__)  # Where to put all the generated files
        self.tournament_name = ""  # Name of tournament
        self.introductory_text = ""  # Text to be displayed to the coaches
        self.contest_weight_text = "" # Text to describe contest weighting system
        self.awards = []
        self.max_num_students = 0
        self.min_num_students = 0
        self.individual_weight = 0

        # sets values for above csvs, csvs, and contest and other variables above
        self.initialize()

        # fills in values for self.teams and self.students and contest scores
        self.parse_overall_data(self.team_csv)
        self.parse_individual_data(self.individual_csv)

        for contest in self.team_contests:
            self.parse_team_contest_data(contest)

        for contest in self.individual_contests:
            self.parse_individual_contest_data(contest)
        self.remove_no_shows()
        self.calculate_ranks()

    @abstractmethod
    def initialize(self):
        """
        Fills in values for various class variables namely:
            self.overall_csv
            self.individual_csv
            self.team_contests
            self.individual_contests
            self.statistics
            self.output_dir
            self.tournament_name
            self.introductory_text
        """
        pass

    def do(self):
        self.output_overall_csv()
        print("Outputted Overall CSV!")
        self.give_awards()
        print("Finished Generating Awards")
        self.generate_score_reports()
        print("Finished Generating Score Reports")
        self.combine_awards()
        print("Finished Combining Awards With Score Reports")

    @staticmethod
    def read_commands(csv_file):
        """
        Read the new commands from csvFile, and return them as an OrderedDict, with
        the first row of the file as keys.
        """
        with open(csv_file, 'r') as csv_f:
            csv_iterator = csv.reader(csv_f)
            keys = next(csv_iterator)
            return [OrderedDict((k, v) for k, v in zip(keys, line)) for line in csv_iterator]

    def parse_overall_data(self, overall_csv):
        """
        Parses the csv and generates teams
        :param overall_csv: CSV with all the team data inside of it (from Ag)
        :return: Sets the variable self.teams
        """
        teams = BaseController.read_commands(overall_csv)

        for team in teams:
            team_info_parsed = self.parse_team_info(team)
            team_to_add = Team(self.max_num_students, self.min_num_students, self.individual_weight, **team_info_parsed)
            self.teams[team_to_add.id] = team_to_add

    def remove_no_shows(self):
        idx_to_remove = []
        for idx, team in self.teams.items():
            total_contests = team.contests[:]
            team.students = [student for student in team.students if len(student.contests) > 0]
            for student in team.students:
                total_contests.extend(student.contests)
            if len(total_contests) == 0:
                idx_to_remove.append(idx)
        for idx_1 in idx_to_remove:
            del self.teams[idx_1]

    def parse_individual_data(self, indiv_csv):
        """
        Parses individual csvs and sets the value of self.students and adds students to teams
        :param indiv_csv: CSV
        """
        individuals = BaseController.read_commands(indiv_csv)

        for indiv in individuals:
            indiv_info_parsed = self.parse_individual_info(indiv)
            indiv_to_add = Student(**indiv_info_parsed)
            self.students[indiv_to_add.id] = indiv_to_add
            team = self.teams[indiv_to_add.team_id]
            assert team.is_on_team(indiv_to_add.id)
            team.add_student(indiv_to_add)

    def parse_team_contest_data(self, contest):
        """
        Takes data from a specific contest and parses it, setting values for the contests variable in each team.
        :param contest: Contest to be parsed
        """

        teams_data = BaseController.read_commands(contest.csv)

        for team_data in teams_data:
            contest_info_parsed = self.parse_team_contest_info(team_data, contest)
            assert 'id' in contest_info_parsed.keys()
            team = self.teams[contest_info_parsed.pop('id')]
            contest_score = ContestScore(contest, team, is_team=True, **contest_info_parsed)
            if contest_score.has_score:
                assert len(contest_score.scores) == contest.num_problems
            team.add_contest(contest_score)
            contest.add_score(contest_score)

    def parse_individual_contest_data(self, contest):
        """
        Takes an individual contest an parses it, setting the contests attribute for the student variables
        :param contest: Contest to be parsed
        """

        indivs_data = BaseController.read_commands(contest.csv)

        for individual_data in indivs_data:
            contest_info_parsed = self.parse_individual_contest_info(individual_data, contest)
            assert 'id' in contest_info_parsed.keys()
            student = self.students[contest_info_parsed.pop('id')]
            contest_score = ContestScore(contest, student, is_team=False, **contest_info_parsed)
            assert len(contest_score.scores) == contest.num_problems
            student.add_contest(contest_score)
            contest.add_score(contest_score)

    def compute_rank(self):
        teams = list(self.teams.values())
        teams.sort(reverse=True)
        curr_rank = 0
        curr_score = -1
        for idx, team in enumerate(teams):
            if team.overall_score != curr_score:
                curr_rank = idx + 1
            curr_score = team.overall_score
            team.set_rank(curr_rank)

    @abstractmethod
    def parse_team_info(self, team_info):
        """
        :param team_info: dictionary with csv information
        :return: A dictionary with format: {"id": Team ID, "name": team name }
        """
        return dict(team_info)

    @abstractmethod
    def parse_individual_info(self, team_info):
        """
        :param team_info: dictionary with csv information. Note individuals ID is team ID plus a single character!
        :return: A dictionary with format: {"id": Individual ID, "name": individual name }
        """
        return dict(team_info)

    @abstractmethod
    def parse_team_contest_info(self, team_info, contest):
        """
        Gets the right data given a row and contest
        :param team_info: Single CSV Row
        :param contest: Contest to be parsed
        :return: a dictionary with the format {"id": Team ID, "scores": [list of scores] or
        "total_score": total score}
        """
        return dict(team_info)

    @abstractmethod
    def parse_individual_contest_info(self, team_info, contest):
        """
        Gets the right data given a row and contest
        :param team_info: Single CSV Row
        :param contest: Contest to be parsed
        :return: a dictionary with the format {"id": Individual ID, "scores": [list of scores] or
        "total_score": total score }
        """
        return dict(team_info)

    def calculate_ranks(self):
        for contest in self.team_contests:
            contest.compute_rank()
        for contest in self.individual_contests:
            contest.compute_rank()
        for team in self.teams.values():
            team.compute_overall()
        self.compute_rank()

    def get_teams_with_rank(self, rank):

        return [team for team in self.teams.values() if team.overall_rank == rank]

    def give_awards(self):
        for award in self.awards:
            award.give_awards()

    def give_single_award(self):
        """
        For debugging purposes, generates a single award.
        """
        for award in self.awards:
            award.give_single_award()

    def output_overall_csv(self):
        with open(f"{self.output_dir}/team.csv", "w") as csvfile:
            file_writer = csv.writer(csvfile)
            file_writer.writerow(["Team", "Score", "Rank", "ID"])
            teams = list(self.teams.values())
            teams.sort(key=lambda team: -team.overall_score)
            for team in teams:
                file_writer.writerow([team.name, team.overall_score, team.overall_rank, team.id])

    def generate_score_reports(self):
        """
        Uses the pylatex package to generate a PDF for each team
        """
        scans_to_download = []  # scans to be downloaded from Ag
        documents_to_compile = []  # tex files to be generated

        for team in self.teams.values():
            file_path = str(self.output_dir) + f"/{team.id}_score_report"
            team.score_report_path = file_path + '.pdf'
            if os.path.exists(team.score_report_path) and not int(team.id) == 14:
                continue
            document = Document(file_path)
            self.setup_document(document)
            self.write_cover_sheet(document, team)
            document.append(NewPage())
            for contest_score in team.contests:
                if not contest_score.has_score:
                    continue
                if contest_score.has_scan:
                    # finds and appends scans for tests
                    contest_name = '_'.join(contest_score.contest.name.split(" "))
                    scan_name = f"{team.id}_{contest_name}_scan.jpg"
                    scan_name = os.path.join(self.output_dir, scan_name)
                    if not os.path.exists(scan_name):
                        scans_to_download.append((scan_name, contest_score.scan_url))
                BaseController.write_contest(document, team, contest_score)
                document.append(NewPage())

            for student in team.students:
                for contest_score in student.contests:
                    if contest_score.has_scan:
                        # finds and appends scans for tests
                        contest_name = '_'.join(contest_score.contest.name.split(" "))
                        scan_name = f"{student.id}_{contest_name}_scan.jpg"
                        scan_name = os.path.join(self.output_dir, scan_name)
                        if not os.path.exists(scan_name):
                            scans_to_download.append((scan_name, contest_score.scan_url))
                    BaseController.write_contest(document, student, contest_score)
                    document.append(NewPage())

            documents_to_compile.append(document)

        BaseController.download_scans(scans_to_download)
        BaseController.compile_pdfs(documents_to_compile)

    def combine_awards(self):

        for team in self.teams.values():
            out_path = team.score_report_path[:-4] + "_FINAL.pdf"
            if os.path.exists(out_path):
                continue
            awards = team.awards
            for student in team.students:
                awards.extend(student.awards)
            merger = PdfFileMerger()
            merger.append(team.score_report_path)
            for pdf in awards:
                merger.append(pdf)
            merger.write(out_path)
            merger.close()


    @staticmethod
    def compile_pdfs(tex_files):
        """
        Compliles latex pdf
        :param tex_files: List of tex files to compile
        """

        for idx, file in enumerate(tex_files):
            file.generate_pdf(clean=True, clean_tex=False)
            print(f"Generated file {idx + 1} of {len(tex_files)}")

    @staticmethod
    def download_scans(scans):
        """
        Uses python subprocess to multi-thread and quickly download scans from Ag
        :param scans: List of tuples of form (scan name, scan url) to download
        """
        processes = []
        for name, scan in scans:
            p = subprocess.Popen(['wget', '-O', name, scan])
            processes.append(p)
        for process in processes:
            process.wait()

    @staticmethod
    def write_contest(document, contest_score_entity, contest_score):
        if not contest_score.contest.contains_answers():
            BaseController.write_contest_without_answers(document, contest_score_entity, contest_score)
        else:
            BaseController.write_contest_with_answers(document, contest_score_entity, contest_score)

    @staticmethod
    def write_contest_with_answers(document, contest_score_entity, contest_score):
        """
        Writes the latex page for a specific contest
        :param document: Pylatex Document
        :param contest_score_entity: Either a team or individual (must have id and name fields)
        :param contest_score: Contest Score object for a given team for a given contest
        :return:
        """

        with document.create(Section(f"{contest_score.contest.name} Results", numbering=False, label=False)):

            # Heading for the top of the section
            heading_line1 = f"\\bf \\large ID:  {contest_score_entity.id}"
            heading_line2 = f"\\bf \\large Name: {contest_score_entity.name}"
            join = '\hfill'
            document.append(NoEscape('{' + heading_line1 + '}' + join + '{' + heading_line2 + '}'))

            # Table for the results display
            with document.create(Center()):
                with document.create(Tabular('||c|c|c|c||')) as data_table:
                    data_table.add_hline()
                    data_table.add_hline()
                    data_table.add_row([bold("Problem Number"), bold("Correct Answer"),
                                        bold("Student Answer"), bold("Points Received")])
                    data_table.add_hline()
                    data_table.add_hline()
                    for idx, score in enumerate(contest_score.scores):
                        answer_val = contest_score.contest.answers[idx]
                        if type(answer_val) == list:
                            answer_val = " or ".join([str(answer) for answer in answer_val])
                        curr_row = [idx + 1, answer_val, contest_score.answers[idx], score]
                        data_table.add_row(curr_row)
                        data_table.add_hline()
                    data_table.add_hline()

            # Bottom label for the final score and rank
            total_score = f"\\bf \\large Total Score:  {contest_score.total_score}"
            rank = f"\\bf \\large Rank: {contest_score.rank}"
            document.append(NoEscape('{' + total_score + '}'))
            document.append(NoEscape(r"\hfill"))
            document.append(NoEscape('{' + rank + '}'))

        # Page for Ag Scan if available
        if contest_score.has_scan:
            document.append(NewPage())
            with document.create(Figure(position='h!')) as scan_image:
                contest_name = '_'.join(contest_score.contest.name.split(" "))
                scan_image.add_image(f"{contest_score_entity.id}_{contest_name}_scan.jpg",
                                     width=NoEscape(r"0.95\textwidth"))


    @staticmethod
    def write_contest_without_answers(document, contest_score_entity, contest_score):
        """
        Writes the latex page for a specific contest
        :param document: Pylatex Document
        :param contest_score_entity: Either a team or individual (must have id and name fields)
        :param contest_score: Contest Score object for a given team for a given contest
        :return:
        """

        with document.create(Section(f"{contest_score.contest.name} Results", numbering=False, label=False)):

            # Heading for the top of the section
            heading_line1 = f"\\bf \\large ID:  {contest_score_entity.id}"
            heading_line2 = f"\\bf \\large Name: {contest_score_entity.name}"
            join = '\hfill'
            document.append(NoEscape('{' + heading_line1 + '}' + join + '{' + heading_line2 + '}'))

            # Table for the results display
            with document.create(Center()):
                with document.create(Tabular('||c|c|c|c||')) as data_table:
                    data_table.add_hline()
                    data_table.add_hline()
                    curr_head_row = [""] * 4
                    curr_row = [""] * 4
                    for idx, score in enumerate(contest_score.scores):
                        mod4 = idx % 4
                        curr_head_row[mod4] = bold(f"Problem {idx + 1}")
                        curr_row[mod4] = contest_score.scores[idx]
                        if mod4 == 3:
                            data_table.add_row(curr_head_row)
                            data_table.add_hline()
                            data_table.add_row(curr_row)
                            data_table.add_hline()
                            curr_head_row = [""] * 4
                            curr_row = [""] * 4
                    if len(contest_score.scores) % 4 != 0:
                        data_table.add_row(curr_head_row)
                        data_table.add_hline()
                        data_table.add_row(curr_row)
                        data_table.add_hline()
                    data_table.add_hline()

            # Bottom label for the final score and rank
            total_score = f"\\bf \\large Total Score:  {contest_score.total_score}"
            rank = f"\\bf \\large Rank: {contest_score.rank}"
            document.append(NoEscape('{' + total_score + '}'))
            document.append(NoEscape(r"\hfill"))
            document.append(NoEscape('{' + rank + '}'))

        # Page for Ag Scan if available
        if contest_score.has_scan:
            document.append(NewPage())
            with document.create(Figure(position='h!')) as scan_image:
                contest_name = '_'.join(contest_score.contest.name.split(" "))
                scan_image.add_image(f"{contest_score_entity.id}_{contest_name}_scan.jpg",
                                     width=NoEscape(r"0.95\textwidth"))

    def setup_document(self, document):
        """
        Sets up preamble to the latex document. Uses scorereports.sty (located 3 directories above in the root).
        :param document: pylatex document
        """
        document.preamble.append(Package("../../../scorereports"))
        document.preamble.append(Package("pdfpages"))
        document.preamble.append(Command('lhead', arguments=f'{self.tournament_name}'))
        document.preamble.append(NoEscape(r'\onehalfspacing'))

    def write_cover_sheet(self, document, team):
        """
        Writes the cover sheet for BMT Score reports
        :param document: pylatex document
        :param team: team that is being written about
        """

        # ********************* Introductory text **********************
        document.append(Command('univlogo'))
        document.append(NoEscape(r" {\Huge " + f"{self.tournament_name} Score Reports" + r" } \\ "))
        document.append(NoEscape(r"""
            Dear Coach,  \\
            """
                                 + self.introductory_text +
                                 f"""
            Attached you will find the score reports for your team. Below are the overall
            statistics for this year's {self.tournament_name}.

            Thank You!
        """))

        # ***************************** Overall Statistics ****************
        contests = self.team_contests + self.individual_contests

        # Generate data table
        with document.create(Section("Overall Statistics", numbering=False, label=False)):
            contest_split = np.array_split(contests, len(contests)//3)
            for contest_group in contest_split:
                num_cs = '||' + '|'.join(['c' for _ in range(len(contest_group) + 1)]) + '||'
                with document.create(Center()):
                    with document.create(Tabular(num_cs)) as data_table:
                        data_table.add_hline()
                        data_table.add_hline()
                        data_table.add_row([bold('Statistic')] + [bold(contest.name) for contest in contest_group])
                        data_table.add_hline()
                        data_table.add_hline()
                        data_table.add_row([bold('Contest Weight (See Note)')] + [np.round(contest.weight, 3) for contest in contest_group])
                        data_table.add_hline()
                        data_table.add_row([bold('Weight Per Point')] + [np.round(contest.point_weight, 3) for contest in contest_group])
                        data_table.add_hline()
                        for statistic in self.statistics:
                            data_table.add_row([bold(statistic.name)] + [statistic.compute(contest.get_all_scores())
                                                                         for contest in contest_group])
                            data_table.add_hline()
                        data_table.add_hline()

        if self.contest_weight_text:
            document.append(NoEscape(bold("Note: ") + self.contest_weight_text))
        document.append(NewPage())

        # **************************** Team Summary ****************************
        with document.create(Center()):
            name = f"\\Huge {team.name} Score Report"
            document.append(NoEscape('{' + name + '}'))

        with document.create(Section("Team Summary", numbering=False, label=False)):
            with document.create(Center()):
                num_cs = '||' + '|'.join(['c' for _ in range(len(team.contests) + 1)]) + '||'
                with document.create(Tabular(num_cs)) as data_table:
                    data_table.add_hline()
                    data_table.add_hline()
                    header_row = [bold('Contest')] + [bold(contest.contest.name) for contest in team.contests]
                    data_table.add_row(header_row)
                    data_table.add_hline()
                    data_table.add_hline()
                    name_row = [bold('Score')] + [contest.total_score for contest in team.contests]
                    data_table.add_row(name_row)
                    data_table.add_hline()
                    rank_row = [bold('Rank')] + [contest.rank for contest in team.contests]
                    data_table.add_row(rank_row)
                    data_table.add_hline()
                    overall_row = [bold('Score Contribution')] + \
                                  [np.round(contest.total_score * contest.contest.point_weight, 3) for contest in team.contests]
                    data_table.add_row(overall_row)
                    data_table.add_hline()
                    data_table.add_hline()

        # ************************* Individual Summary ********************************
        with document.create(Section("Individual Summary", numbering=False, label=False)):
            with document.create(Center()):
                for idx, student in enumerate(team.students):
                    with document.create(Tabular('||c|c||')) as data_table:
                        data_table.add_hline()
                        data_table.add_hline()
                        data_table.add_row([bold("Name"), bold(f"{student.name}")])
                        data_table.add_hline()
                        for contest in student.contests:
                            data_table.add_row([bold(f"{contest.contest.name} Score"), f"{contest.total_score}"])
                            data_table.add_hline()
                            data_table.add_row([bold(f"{contest.contest.name} Rank"), f"{contest.rank}"])
                            data_table.add_hline()
                            overall_row = [bold('Score Contribution'),
                                           f"{np.round(contest.total_score * contest.contest.point_weight * self.individual_weight / 100 / max(self.min_num_students, len(team.students)), 3)}"]
                            data_table.add_row(overall_row)
                            data_table.add_hline()
                            data_table.add_hline()
                    document.append(VerticalSpace("10pt"))
                    document.append(LineBreak())

        # ********************************* Rankings *****************************
        # Bottom label for the final score and rank
        total_score = f"\\bf \\large Total Score:  {np.round(team.overall_score, 3)}"
        rank = f"\\bf \\large Rank: {team.overall_rank}"
        document.append(NoEscape('{' + total_score + '}'))
        document.append(NoEscape(r"\hfill"))
        document.append(NoEscape('{' + rank + '}'))
