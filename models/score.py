
class ContestScore:
    """
    Object representing a single score for a single team/individual on a single round
    """

    def __init__(self, contest, team, is_team=True, **kwargs):
        """
        :param team: Corresponding team
        :param contest: Corresponding round
        :param kwargs: dictionary with  one of ['scores'] or ['total_score'] and maybe ['scan_url'] for images
        """
        assert 'total_score' in kwargs.keys() or 'scores' in kwargs.keys(), \
            "Must get total score or scores list from csv"
        self.is_team = is_team
        self.team = team
        self.scan_url = None
        self.contest = contest
        self.scores = None
        self.rank = None
        self.answers = None
        if 'scan_url' in kwargs.keys():
            self.scan_url = kwargs.pop('scan_url')

        if 'scores' in kwargs.keys():
            self.scores = kwargs.pop('scores')

        if 'total_score' in kwargs.keys():
            self.total_score = float(kwargs.pop('total_score'))
        else:
            self.total_score = sum(self.scores)

        if 'answers' in kwargs.keys():
            self.answers = kwargs.pop('answers')
        self.extra_info = kwargs

    @property
    def has_score(self):
        return bool(self.scores)

    @property
    def has_scan(self):
        return bool(self.scan_url)

    def set_rank(self, rank):
        self.rank = rank

    def __eq__(self, other):
        return self.total_score == other.total_score

    def __lt__(self, other):
        return self.total_score < other.total_score

    def __ne__(self, other):
        return self.total_score != other.total_score

    def __gt__(self, other):
        return self.total_score > other.total_score

    def __ge__(self, other):
        return self.total_score >= other.total_score

    def __le__(self, other):
        return self.total_score <= other.total_score

