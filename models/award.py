from abc import ABC, abstractmethod
from utils import ThreshHoldStrategy, get_percentile, rank_to_phrase
from pathlib import Path
import os
from latex2pdf import replace_new_commands, generate_pdf


class AwardDocument(ABC):

    def __init__(self, award_name, path_to_award):
        """
        :param award_name: name of award
        :param path_to_award: The document associated with the award
        """
        self.name = award_name
        self.path_to_award = path_to_award

    @abstractmethod
    def give_awards(self):
        """
        Adds a set of paths of award files to the appropriate team award path list (eg: team.awards)
        """
        pass

    @abstractmethod
    def give_single_award(self):
        """
        Adds a set of paths of award files to the appropriate team award path list (eg: team.awards)
        """
        pass


class CutoffAward(AwardDocument):

    """
    Award representing that the top - x sutdents get latex_docs
    """
    def __init__(self, award_name, contest, path_to_award, cutoff,
                 is_percentage=True, strategy=ThreshHoldStrategy.EXCLUDE):
        """
            :param award_name: name of award
            :param contest: The round associated with the award
            :param path_to_award: The document associated with the award
            :param cutoff: the top percentage or number of people who get the award in the given round
            :param is_percentage: whether or not the document is for percentage
            :param strategy: Strategy on breaking ties and including them in latex_docs
        """
        super(CutoffAward, self).__init__(award_name, path_to_award)
        self.contest = contest
        self.is_percentage = is_percentage
        self.cutoff = cutoff
        self.strategy = strategy

    def give_awards(self):
        relevant_scores = get_percentile(self.contest.scores, self.cutoff, is_percent=self.is_percentage, strategy=self.strategy)
        for score in relevant_scores:
            award_path = self.generate_award(score)
            score.team.awards.append(award_path)

    def give_single_award(self):
        relevant_scores = get_percentile(self.contest.scores, self.cutoff, is_percent=self.is_percentage, strategy=self.strategy)
        for score in relevant_scores:
            award_path = self.generate_award(score)
            score.team.awards.append(award_path)
            break

    def generate_award(self, score):
        """ Generates an award for a given score """
        return self.path_to_award


class OverallCutoffAward(AwardDocument):

    """
    Award representing that the top - x students get latex_docs in the overall tournament
    """
    def __init__(self, award_name, controller, path_to_award, cutoff,
                 is_percentage=True, strategy=ThreshHoldStrategy.EXCLUDE):
        """
            :param award_name: name of award
            :param contest: The round associated with the award
            :param path_to_award: The document associated with the award
            :param cutoff: the top percentage or number of people who get the award in the given round
            :param is_percentage: whether or not the document is for percentage
            :param strategy: Strategy on breaking ties and including them in latex_docs
        """
        super(OverallCutoffAward, self).__init__(award_name, path_to_award)
        self.controller = controller
        self.is_percentage = is_percentage
        self.cutoff = cutoff
        self.strategy = strategy

    def give_awards(self):
        teams = list(self.controller.teams.values())
        relevant_scores = get_percentile(teams, self.cutoff, is_percent=self.is_percentage, strategy=self.strategy,
                                         key=lambda tm: tm.overall_score)
        for team in relevant_scores:
            award_path = self.generate_award(team)
            team.awards.append(award_path)

    def give_single_award(self):
        teams = list(self.controller.teams.values())
        relevant_scores = get_percentile(teams, self.cutoff, is_percent=self.is_percentage, strategy=self.strategy,
                                         key=lambda tm: tm.overall_score)
        for team in relevant_scores:
            award_path = self.generate_award(team)
            team.awards.append(award_path)
            break

    def generate_award(self, team):
        return self.path_to_award


class PlacementAward(AwardDocument):
    """
    Award represent specific placement in a contest (e.g first, second, etc)
    """

    def __init__(self, award_name, contest, path_to_award, ranks):
        """
            :param award_name: name of award
            :param contest: The round associated with the award
            :param path_to_award: The document associated with the award
            :param ranks in the contests that receive the award - 0 indexed
        """
        super(PlacementAward, self).__init__(award_name, path_to_award)
        self.contest = contest
        self.ranks = ranks

    def give_awards(self):
        relevant_scores = []
        for rank in self.ranks:
            relevant_scores.extend(self.contest.get_scores_with_rank(rank))

        for score in relevant_scores:
            award_path = self.generate_award(score)
            score.team.awards.append(award_path)

    def give_single_award(self):
        relevant_scores = []
        for rank in self.ranks:
            relevant_scores.extend(self.contest.get_scores_with_rank(rank))

        for score in relevant_scores:
            award_path = self.generate_award(score)
            score.team.awards.append(award_path)
            break

    def generate_award(self, score):
        return self.path_to_award


class OverallPlacementAward(AwardDocument):
    """
    Award represent specific placement in a contest (e.g first, second, etc)
    """

    def __init__(self, award_name, controller, path_to_award, ranks):
        """
            :param award_name: name of award
            :param controller: The overall contest associated with the award
            :param path_to_award: The document associated with the award
            :param ranks in the contests that receive the award - 0 indexed
        """
        super(OverallPlacementAward, self).__init__(award_name, path_to_award)
        self.ranks = ranks
        self.controller = controller

    def give_awards(self):
        relevant_teams = []
        for rank in self.ranks:
            relevant_teams.extend(self.controller.get_teams_with_rank(rank))

        for team in relevant_teams:
            award_path = self.generate_award(team)
            team.awards.append(award_path)

    def give_single_award(self):
        relevant_teams = []
        for rank in self.ranks:
            relevant_teams.extend(self.controller.get_teams_with_rank(rank))

        for team in relevant_teams:
            award_path = self.generate_award(team)
            team.awards.append(award_path)
            break

    def generate_award(self, team):
        return self.path_to_award


class LaTeXCutoffAward(CutoffAward):

    """
    Award that represents a cutoff award for latex specifically. This is useful for taking a given latex template and
    converting that template into an award that represents a cutoff for a score (e.g: certificates)
    """

    def __init__(self, award_name, contest, path_to_base_award, variables, cutoff,
                 is_percentage=True, strategy=ThreshHoldStrategy.EXCLUDE):

        super(LaTeXCutoffAward, self).__init__(award_name, contest, path_to_base_award, cutoff, is_percentage, strategy)
        self.variables = variables

    def generate_award(self, score):
        context = score.team.extra_info
        context.update({"ID": score.team.id, "Name": score.team.name, "ContestName": self.contest.name,
                        "AwardName": self.name})
        latex_document = self.path_to_award
        with open(latex_document) as file:
            tex = file.read()

        path = Path(latex_document)
        file_name = "_".join((self.contest.name + "_" + self.name).split(" "))
        file_path = str(path.parent.joinpath(f'{score.team.id}_{file_name}'))
        if os.path.exists(f"{file_path}.pdf"):
            return file_path + ".pdf"

        with open(f"{file_path}.tex", 'w') as fp:
            # replaces "new command variables" in the tex document with the given context for that document.
            new_tex = replace_new_commands(tex, context)
            fp.write(new_tex)

        # generates the pdf in the correct file path. Takes out original tex file and any aux files.
        generate_pdf(file_path, clean_tex=True, clean=True)
        return file_path + ".pdf"


class LaTeXOverallCutoffAward(OverallCutoffAward):
    """
    Award that represents a cutoff award for latex specifically. This is useful for taking a given latex template and
    converting that template into an award that represents a cutoff for the overall tournament (e.g: certificates)
    """

    def __init__(self, award_name, controller, path_to_base_award, variables, cutoff,
                 is_percentage=True, strategy=ThreshHoldStrategy.EXCLUDE):

        super(LaTeXOverallCutoffAward, self).__init__(award_name, controller, path_to_base_award,
                                                      cutoff, is_percentage, strategy)
        self.variables = variables

    def generate_award(self, team):
        context = team.extra_info
        context.update({"ID": team.id, "Name": team.name, "ContestName": self.controller.tournament_name,
                        "AwardName": self.name})
        latex_document = self.path_to_award
        with open(latex_document) as file:
            tex = file.read()

        path = Path(latex_document)
        file_name = "_".join((self.controller.tournament_name + "_Overall_" + self.name).split(" "))
        file_path = str(path.parent.joinpath(f'{team.id}_{file_name}'))
        if os.path.exists(f"{file_path}.pdf"):
            return file_path + '.pdf'

        with open(f"{file_path}.tex", 'w') as fp:
            new_tex = replace_new_commands(tex, context)
            fp.write(new_tex)

        generate_pdf(file_path, clean_tex=True, clean=True)
        return file_path + '.pdf'


class LaTeXPlacementAward(PlacementAward):
    """
    Award that represents a cutoff award for latex specifically. This is useful for taking a given latex template and
    converting that template into an award that represents a cutoff for the overall tournament (e.g: certificates)
    """

    def __init__(self, award_name, controller, path_to_base_award, variables, ranks):

        super(LaTeXPlacementAward, self).__init__(award_name, controller, path_to_base_award, ranks)
        self.variables = variables

    def generate_award(self, score):
        context = score.team.extra_info
        name = f"{rank_to_phrase(score.rank)} Place"
        context.update({"ID": score.team.id, "Name": score.team.name, "ContestName": self.contest.name,
                        "AwardName": name })
        latex_document = self.path_to_award
        with open(latex_document) as file:
            tex = file.read()

        path = Path(latex_document)
        file_name = "_".join((self.contest.name + "_" + name).split(" "))
        file_path = str(path.parent.joinpath(f'{score.team.id}_{file_name}'))
        if os.path.exists(f"{file_path}.pdf"):
            return file_path + ".pdf"

        with open(f"{file_path}.tex", 'w') as fp:
            # replaces "new command variables" in the tex document with the given context for that document.
            new_tex = replace_new_commands(tex, context)
            fp.write(new_tex)

        # generates the pdf in the correct file path. Takes out original tex file and any aux files.
        generate_pdf(file_path, clean_tex=True, clean=True)
        return file_path + ".pdf"


class LaTeXOverallPlacementAward(OverallPlacementAward):
    """
    Award that represents a cutoff award for latex specifically. This is useful for taking a given latex template and
    converting that template into an award that represents a cutoff for the overall tournament (e.g: certificates)
    """

    def __init__(self, award_name, controller, path_to_base_award, variables, ranks):

        super(LaTeXOverallPlacementAward, self).__init__(award_name, controller, path_to_base_award, ranks)
        self.variables = variables

    def generate_award(self, team):
        context = team.extra_info
        name = f"{rank_to_phrase(team.overall_rank)} Place"
        context.update({"ID": team.id, "Name": team.name, "ContestName": self.controller.tournament_name,
                        "AwardName": name})
        latex_document = self.path_to_award
        with open(latex_document) as file:
            tex = file.read()

        path = Path(latex_document)
        file_name = "_".join((self.controller.tournament_name + "_Overall_" + name).split(" "))
        file_path = str(path.parent.joinpath(f'{team.id}_{file_name}'))
        if os.path.exists(f"{file_path}.pdf"):
            return file_path + '.pdf'

        with open(f"{file_path}.tex", 'w') as fp:
            new_tex = replace_new_commands(tex, context)
            fp.write(new_tex)

        generate_pdf(file_path, clean_tex=True, clean=True)
        return file_path + '.pdf'
