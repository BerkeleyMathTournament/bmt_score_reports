
class Student:
    """
    Object representing a single student
    """

    def __init__(self, **kwargs):
        """
        ID and Name must be apart of kwargs
        :param kwargs: Dictionary with at least id and name of student
        """
        assert 'id' in kwargs.keys()
        assert 'name' in kwargs.keys()

        self.id = kwargs.pop('id')
        self.name = kwargs.pop('name')
        self.extra_info = kwargs
        self.contests = []
        self.awards = []

    def add_contest(self, contest):
        self.contests.append(contest)


    @property
    def team_id(self):
        return self.id[:-1]
