from utils import top_ten_avg, top_ten_percent_avg


class Contest:
    """
    Object representing a BMT competition, this is usually set by the score report generator!
    """

    def __init__(self, num_problems, total_points, name, weight, csv, answers=None):
        self.name = name  # name of round
        self.num_problems = num_problems  # number of problems
        self.scores = []  # list of round scores
        self.total_points = total_points # total number of points in the round
        self.weight = weight  # weight of round
        self.csv = csv  # path to csv
        self.point_weight = -1
        self.answers = answers

    def contains_answers(self):
        return self.answers is not None and len(self.answers) == self.num_problems

    def add_score(self, round_score):
        self.scores.append(round_score)

    def get_all_scores(self):
        """
        Returns a list of all scores for each team for statistics purposes
        :return: List of all scores for each team for a given round
        """
        return [score.total_score for score in self.scores]

    def compute_rank(self):
        self.scores.sort(reverse=True)
        curr_rank = 0
        curr_score = -1
        for idx, score in enumerate(self.scores):
            if score.total_score != curr_score:
                curr_rank = idx + 1
            curr_score = score.total_score
            score.set_rank(curr_rank)

    def get_point_weight(self):
        if self.point_weight == -1:
            if self.weight == 0:
                self.point_weight = 0
                return self.point_weight
            if len(self.scores) // 10 < 10:
                weight_norm = top_ten_avg([score.total_score for score in self.scores])
            else:
                weight_norm = top_ten_percent_avg([score.total_score for score in self.scores])
            self.point_weight = self.weight / weight_norm
        return self.point_weight

    def get_scores_with_rank(self, rank):
        return [score for score in self.scores if score.rank == rank]

