class Team:
    """
    Team class representing a BMT Team that competed
    """

    def __init__(self, max_num_students, min_num_students, individual_weight, **kwargs):
        """
        To init, need id, name, overall rank in kwargs amongst other things
        :param kwargs: Dictionary with relevant team information
        """
        assert 'id' in kwargs.keys()
        assert 'name' in kwargs.keys()

        self.id = kwargs.pop('id')
        self.name = kwargs.pop('name')
        self.max_num_students = max_num_students
        self.min_num_students = min_num_students
        self.individual_weight = individual_weight
        self.students = []
        self.overall_rank = -1
        self.overall_score = -1
        self.extra_info = kwargs
        self.contests = []
        self.awards = []
        self.score_report_path = None

    def add_student(self, student):
        self.students.append(student)

    def is_on_team(self, stud_id):
        """
        Student ids are Team Ids plus an extra character
        :param stud_id: ID of student
        """
        return stud_id[:-1] == self.id

    def add_contest(self, contest):
        self.contests.append(contest)

    def set_rank(self, rank):
        self.overall_rank = rank

    def compute_overall(self):
        total_score = 0
        for contest in self.contests:
            total_score += contest.total_score * contest.contest.get_point_weight()
        student_contrib_scores = []
        for student in self.students:
            student_contrib = 0
            for contest in student.contests:
                student_contrib += contest.total_score * contest.contest.get_point_weight()
            student_contrib_scores.append(student_contrib)

        student_contrib = sum(student_contrib_scores) / max(self.min_num_students, len(self.students))
        total_score += student_contrib / 100 * self.individual_weight

        self.overall_score = total_score

    def __eq__(self, other):
        return self.overall_score == other.overall_score

    def __lt__(self, other):
        return self.overall_score < other.overall_score

    def __ne__(self, other):
        return self.overall_score != other.overall_score

    def __gt__(self, other):
        return self.overall_score > other.overall_score

    def __ge__(self, other):
        return self.overall_score >= other.overall_score

    def __le__(self, other):
        return self.overall_score <= other.overall_score
