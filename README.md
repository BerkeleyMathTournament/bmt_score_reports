# BMT Score Report Generating Scripts

This repository is a set of scripts designed to take CSV's generated for math competitions
and convert them into nicely written score reports. To be used by BMT only as well as any
potential partners. 


## Assumptions

The repository makes the following assumptions: 

- There are csvs for each round (not necessarily distinct) that contain important information
- There is a team csv with an ID, Team Name, (the column names do not need to be exact)
- There is an individual csv with the ID and Name of each individual. The ID of the student is the ID of the team plus one character
- The Team ID field in overall is present in all other CSVs for each team round
- For any individual round, there is also an ID field with. 
- Some CSVs may or may not have scans (Image URLs) or problem results for each individual problem on the test
- Any extra information for certificates is contained within these CSVs 


## Usage

To use this repository, you need to write a configuration script for each tournament you wish to generate
score reports for. The repository structure is as follows

```
.
├── scorereports.sty      # Style File for your document (customize accordingly)
|-- logo.png                # Logo File for your tournament 
├── competitions              
│   ├── example_comp        # Example Competition
│   |    |-- output_dir     # Where output pdfs / jpgs / tex files go
│   |    |-- csvs           # Where input CSV files go
|   |    |-- example.py     # Config file for the competition
|   └── ...                 # Other competitions
|-- statistic.py            # Statistic models for rounds
|-- utils.py                # Util methods for arbitrary computations
|-- latex2pdf.py            # Method for replacing commands with given context and compiling a latex doc into a pdf 
|-- models        
    |-- award.py            # Award Models for different types of awards
    |-- contest.py          # Models for specific contests (e.g team / puzzle) 
    |-- score.py            # Models for a given score for a given round
    |-- student.py          # Models for a given student for a team
    |-- team.py             # Model for a given team of a round        
|-- base_controller.py      # Models for the base controller class that generates the score reports
|-- latex_docs        
    |-- certificate         # Folder that contains tex for giving out certificates.   
```

To use this repository, create a directory for your competition in competitions and follow the stucture above creating
directories `output_dir`, `csvs` for your input and outputs as well as a python file for your configuration. 

The python file, should have two main parts: 
   - A subclass of `BaseController` in `BaseController.py`
   - Command Line interface (i.e `if __name__ == '__main__': `)

To get started, I recommend taking one of the example configurations and massaging it to fit your tournament!
Other important files are the `Round` class in `score_reports.py` since you will be directly creating objects 
in that class. 

## Development

This repository uses the `pylatex` to generate score reports. This means all of the `LaTeX` Code is 
in python (except for styling). Please be wary of this when/if you change the `BaseController.py`. 

In addition to this, there is a system for generating awards systematically. If awards are already generated, it will
skip over the regeneration step (since it is quite time costly). If you want to regenerate that award, you should 
delete it. 
