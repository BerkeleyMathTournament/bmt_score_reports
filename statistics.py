import numpy as np
from enum import Enum
from utils import top_ten_avg, top_ten_percent_avg


class Statistic:

    def __init__(self, name, compute_fn):
        self.name = name
        self.compute = compute_fn


# common statistics
class StatisticEnum(Enum):
    MAX = Statistic('Maximum', lambda x: max(x))
    MIN = Statistic('Minimum', lambda x: min(x))
    AVG = Statistic('Average', lambda x: np.round(np.mean(x), 3))
    FIRST_QRT = Statistic('First Quartile', lambda x: np.percentile(x, 25))
    MEDIAN = Statistic('Median', lambda x: np.percentile(x, 50))
    THIRD_QRT = Statistic('Third Quartile', lambda x: np.percentile(x, 75))
    ST_DEV = Statistic('Standard Deviation', lambda x: np.round(np.std(x), 3))
    COUNT = Statistic('Count', lambda x: len(x))
    HONORABLE_MENTION_CUTOFF = Statistic('Honorable Mention Cutoff', lambda x: np.round(np.percentile(x, 80), 3))
    TOP_TEN_PERCENT_AVG = Statistic('Top 10% Avg', lambda x: np.round(top_ten_percent_avg(x), 3))
    TOP_TEN_AVG = Statistic('Top 10 Avg', lambda x: np.round(top_ten_avg(x), 3))
