import numpy as np
from enum import Enum
import math
import inflect

inflect_engine = inflect.engine()

class ThreshHoldStrategy(Enum):
    FIXED = 1 # just take the top x percent without regards to ties
    INCLUDE = 2 # include all with lowest scores that could make it
    EXCLUDE = 3 # exclude all with lowest scores that could make it


# util function for statistics
def top_ten_percent_avg(lst):
    lst.sort(key = lambda x: -x)
    top_ten_thresh = len(lst) // 10
    top_10 = lst[:top_ten_thresh]
    return np.mean(top_10)


def ordinal(n):
    return "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])


def top_ten_avg(lst):
    lst.sort(key = lambda x: -x)
    top_10 = lst[:10]
    return np.mean(top_10)


def get_percentile(scores, top_x, is_percent=True, strategy=ThreshHoldStrategy.EXCLUDE, key=lambda x: x):
    """
    Get top percentile of scores -- such that repeated scores are all included
    :param scores: list of scores (not necessarily sorted)
    :param top_x: either top_x students or top percentile students -- if more than top_x
    :param is_percent: whether top_x is a percent or not
    :param strategy: Strategy as to include or exclude ties or have it remain fixed.
    :return: sorted list of students with score above percentile cutoff
    """
    if len(scores) == 1:
        return scores
    scores.sort(reverse=True, key=key)
    threshold = top_x
    if is_percent:
        threshold = int(math.ceil(top_x * len(scores) / 100))
    top_x_scores = scores[:threshold]

    if strategy == ThreshHoldStrategy.EXCLUDE:
        lowest_score = key(top_x_scores[-1])
        counter = -1
        if key(top_x_scores[-2]) == lowest_score:
            while key(top_x_scores[counter]) == lowest_score:
                top_x_scores.pop()
        return top_x_scores
    elif strategy == ThreshHoldStrategy.INCLUDE:
        lowest_score = key(top_x_scores[-1])
        counter = threshold

        while key(scores[counter]) == lowest_score:
            top_x_scores.append(scores[counter])
            counter += 1
        return top_x_scores
    elif strategy == ThreshHoldStrategy.FIXED:
        return top_x_scores

    else:
        raise NotImplementedError()


def rank_to_phrase(rank):
    return inflect_engine.ordinal(rank).capitalize()



